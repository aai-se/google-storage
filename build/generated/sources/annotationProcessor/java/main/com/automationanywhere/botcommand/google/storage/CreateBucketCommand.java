package com.automationanywhere.botcommand.google.storage;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class CreateBucketCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(CreateBucketCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    CreateBucket command = new CreateBucket();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("bucketName") && parameters.get("bucketName") != null && parameters.get("bucketName").get() != null) {
      convertedParameters.put("bucketName", parameters.get("bucketName").get());
      if(convertedParameters.get("bucketName") !=null && !(convertedParameters.get("bucketName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","bucketName", "String", parameters.get("bucketName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("bucketName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","bucketName"));
    }

    if(parameters.containsKey("location") && parameters.get("location") != null && parameters.get("location").get() != null) {
      convertedParameters.put("location", parameters.get("location").get());
      if(convertedParameters.get("location") !=null && !(convertedParameters.get("location") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","location", "String", parameters.get("location").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("location") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","location"));
    }

    if(parameters.containsKey("storageclass") && parameters.get("storageclass") != null && parameters.get("storageclass").get() != null) {
      convertedParameters.put("storageclass", parameters.get("storageclass").get());
      if(convertedParameters.get("storageclass") !=null && !(convertedParameters.get("storageclass") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","storageclass", "String", parameters.get("storageclass").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("storageclass") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","storageclass"));
    }
    if(convertedParameters.get("storageclass") != null) {
      switch((String)convertedParameters.get("storageclass")) {
        case "STANDARD" : {

        } break;
        case "NEARLINE" : {

        } break;
        case "COLDLINE" : {

        } break;
        case "ARCHIVE" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","storageclass"));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("bucketName"),(String)convertedParameters.get("location"),(String)convertedParameters.get("storageclass")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
