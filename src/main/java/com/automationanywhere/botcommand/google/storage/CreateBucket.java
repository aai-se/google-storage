/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Create Bucket", name = "storagecreatebucket",
        description = "Create Bucket",
        node_label = "Create Bucket {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.DICTIONARY, return_sub_type = DataType.STRING,  return_description = "Keys 'bucketname' , 'location' , 'storageclass' ,'id'" , return_label="Bucket details dictionary", return_required=false)

public class CreateBucket {
	
	  private static final Logger logger = LogManager.getLogger(CreateBucket.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "StorageSession") @NotEmpty String sessionName,
    							   @Idx(index = "2", type = TEXT)  @Pkg(label = "Bucket name" , default_value_type = STRING ) @NotEmpty String bucketName,
      							   @Idx(index = "3", type = TEXT)  @Pkg(label = "Location" , default_value = "us-central1" , default_value_type= STRING ) @NotEmpty String location,
      							   @Idx(index = "4", type = AttributeType.SELECT, options = {
      									@Idx.Option(index = "4.1", pkg = @Pkg(label = "Standard", value = "STANDARD")),
      									@Idx.Option(index = "4.2", pkg = @Pkg(label = "Nearline", value = "NEARLINE")),
      									@Idx.Option(index = "4.3", pkg = @Pkg(label = "Coldline", value = "COLDLINE")),
      									@Idx.Option(index = "4.4", pkg = @Pkg(label = "Archive", value = "ARCHIVE"))}) 
      			    				    @Pkg(label = "Storage class", default_value = "STANDARD", default_value_type = STRING) @NotEmpty String storageclass
    						 ) throws Exception { 
		

		GoogleStorageBucket storage = (GoogleStorageBucket) this.sessions.get(sessionName);  
		HashMap<String, Value> resultMap = new HashMap<String, Value>();
		
		HashMap<String, String> result = storage.createBucketWithStorageClassAndLocation(bucketName, location, storageclass) ;
	    for (Entry<String, String> entry:result.entrySet() ) {
	    	resultMap.put(entry.getKey(), new StringValue(entry.getValue().toString()));
	    }
	  
	    DictionaryValue resultDict = new DictionaryValue();
	    resultDict.set(resultMap);
	    
	    return resultDict;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}