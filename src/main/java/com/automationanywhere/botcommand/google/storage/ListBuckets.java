/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "List Buckets", name = "storagelistbucket",
        description = "List Buckets",
        node_label = "List Buckets {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.LIST, return_sub_type = DataType.STRING , return_description = "List of buckets" , return_label="Buckets", return_required=true)

public class ListBuckets {
	
	  private static final Logger logger = LogManager.getLogger(ListBuckets.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public ListValue<String>  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "StorageSession") @NotEmpty String sessionName
    						 ) throws Exception { 
		
		List<Value> bucketsList = new ArrayList<Value>();
		GoogleStorageBucket storage = (GoogleStorageBucket) this.sessions.get(sessionName);  
		
		List<String> buckets = storage.listBuckets();

		for (Iterator iterator = buckets.iterator(); iterator.hasNext();) {
			String bucket= (String) iterator.next();
			bucketsList.add(new StringValue(bucket));
		}
		
		ListValue<String> resultList = new ListValue<String>();
		resultList.set(bucketsList);
		
	    return resultList;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}