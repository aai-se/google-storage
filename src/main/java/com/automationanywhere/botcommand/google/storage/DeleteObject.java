/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Delete Object", name = "storagedeleteobject",
        description = "Delete Object",
        node_label = "Delete Object {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.STRING,  return_description = "Status of deletion" , return_label="Status", return_required=false)

public class DeleteObject {
	
	  private static final Logger logger = LogManager.getLogger(DeleteObject.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public StringValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "StorageSession") @NotEmpty String sessionName,
    						   @Idx(index = "2", type = TEXT)  @Pkg(label = "Bucket name" , default_value_type = STRING ) @NotEmpty String bucketName,
    						   @Idx(index = "3", type = TEXT)  @Pkg(label = "Object name" , default_value_type = STRING ) @NotEmpty String objectName
    						 ) throws Exception { 
		

		GoogleStorageBucket storage = (GoogleStorageBucket) this.sessions.get(sessionName);  
		
		String status = storage.deleteObject(bucketName, objectName);
	    
	    return new StringValue(status);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}