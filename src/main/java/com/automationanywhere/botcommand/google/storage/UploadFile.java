/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
import static com.automationanywhere.commandsdk.model.DataType.FILE;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Upload File", name = "storageuploadfile",
        description = "Upload File",
        node_label = "Upload File {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type = DataType.STRING,  return_description = "Storage object name" , return_label="Object", return_required=false)

public class UploadFile {
	
	  private static final Logger logger = LogManager.getLogger(UploadFile.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public StringValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "StorageSession") @NotEmpty String sessionName,
    						   @Idx(index = "2", type = TEXT)  @Pkg(label = "Bucket name" , default_value_type = STRING ) @NotEmpty String bucketName,
      						   @Idx(index = "3", type = TEXT)  @Pkg(label = "Folder Path" , description = "Optional folder path" ,default_value_type= STRING )  String folderPath,
      						   @Idx(index = "4", type = TEXT)  @Pkg(label = "Object Name" ,  description = "Optional object name" , default_value_type= STRING )  String objectName,
     						   @Idx(index = "5", type = AttributeType.FILE)  @Pkg(label = "File Path" , default_value_type= FILE ) @NotEmpty String filePath
      						   
    						 ) throws Exception { 
		

		folderPath = (folderPath == null) ? "" : folderPath;
		objectName = (objectName == null) ? "" : objectName;
		GoogleStorageBucket storage = (GoogleStorageBucket) this.sessions.get(sessionName);  
		
		String objID = storage.uploadFile(bucketName, folderPath, objectName, filePath) ;
		
	    return new StringValue(objID);

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}