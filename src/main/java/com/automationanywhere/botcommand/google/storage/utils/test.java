package com.automationanywhere.botcommand.google.storage.utils;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.StringValue;

public class test {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
       GoogleStorageBucket storage = new GoogleStorageBucket();
       storage.auth("C:\\Users\\Stefan Karsten\\Documents\\AlleDateien\\Demos\\Google\\sa-stefan-karsten@gcp-serviceaccounts-keys.iam.gserviceaccount.com.json");

  
       HashMap<String, Object> result = storage.getObjectDetails("aai_bucket_sk", "traindatatables/LoanApprovalPrediction.csv" );
       
       for (Entry<String, Object> entry:result.entrySet() ) {
		  System.out.println(entry.getKey()+" "+entry.getValue());
		  
		  String className = entry.getValue().getClass().getSimpleName() ;
	    	Value value;
	    	switch (className) {
	    		case "ZonedDateTime" :  value = new DateTimeValue((ZonedDateTime)entry.getValue());
	    								break;
	    		case "String" : 		value = new StringValue((String)entry.getValue());
										break;
	    		default: 				value = new StringValue(entry.getValue().toString());
										break;					
		
	    	}
       }
		    storage.downloadObject("aai_bucket_sk", "traindatatables/LoanApprovalPrediction.csv" , "C:\\temp\\test.csv");
		
       }


	}
	