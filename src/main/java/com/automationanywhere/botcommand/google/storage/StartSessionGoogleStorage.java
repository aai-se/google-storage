/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */

package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.bot.service.GlobalSessionContext;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.botcommand.google.storage.utils.GoogleAuth;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.SelectModes;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;





/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start Session", name = "StartGoogleStorageSession", description = "Start new session", 
 icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,node_label = "Start Session {{sessionName}}|") 

public class StartSessionGoogleStorage{
 
    @Sessions
    private Map<String, Object> sessions;
    
	  
	@com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
	  
   private 	GoogleStorageBucket  storage ;
	
    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "StorageSession") @NotEmpty String sessionName,
		         @Idx(index = "2", type = AttributeType.SELECT, options = {
					//Check the index of options. TO make them child we add a "." after parent index and start the indexing from 1.
				    @Idx.Option(index = "2.1", pkg = @Pkg(label = "Auth File", value = "FILE")), 
				    @Idx.Option(index = "2.2", pkg = @Pkg(label = "Auth Parameter", value = "PARA"))})
			           @Pkg(label = "Authentication", description = "", default_value = "FILE", default_value_type = STRING)
					   @SelectModes  @NotEmpty String authtype,
					  
					   @Idx(index = "2.1.1", type = AttributeType.FILE)  @Pkg(label = "Google API Authentication File (JSON)" , default_value_type = DataType.FILE) @NotEmpty  String jsonPath,
		               
		          	   @Idx(index = "2.2.1", type = CREDENTIAL) @Pkg(label = "Project ID",  default_value_type = STRING) @NotEmpty SecureString authprojectID,
		               @Idx(index = "2.2.3", type = CREDENTIAL) @Pkg(label = "Private Key ID",  default_value_type = STRING) @NotEmpty SecureString private_key_id,
		               @Idx(index = "2.2.4", type = CREDENTIAL) @Pkg(label = "Private Key",  default_value_type = STRING) @NotEmpty SecureString private_key,
		               @Idx(index = "2.2.5", type = CREDENTIAL) @Pkg(label = "Client Email",  default_value_type = STRING) @NotEmpty SecureString client_email,
		               @Idx(index = "2.2.6", type = CREDENTIAL) @Pkg(label = "Client ID",  default_value_type = STRING) @NotEmpty SecureString client_id
		              ) throws Exception {

 
        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException("Session name in use ") ;
        
        
        if (authtype.equals("PARA")) {
            String newjsonPath = new GoogleAuth().writeAutFile(authprojectID.getInsecureString(), private_key_id.getInsecureString(), private_key.getInsecureString(), client_email.getInsecureString(), client_id.getInsecureString());
        	this.storage = new GoogleStorageBucket();
        	storage.auth(newjsonPath);
        }
        else {
        	this.storage = new GoogleStorageBucket();
        	storage.auth(jsonPath);
        }



        this.sessions.put(sessionName, this.storage);


 
    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
    
 
    
    
}