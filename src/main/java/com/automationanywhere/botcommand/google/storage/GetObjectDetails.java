/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Get Object Details", name = "storageobjectdetails",
        description = "Get Object Details",
        node_label = "Get Object Details {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.DICTIONARY, return_sub_type = DataType.ANY,  return_description = "Dictionary with object details" , return_label="Object details", return_required=true)

public class GetObjectDetails {
	
	  private static final Logger logger = LogManager.getLogger(GetObjectDetails.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "StorageSession") @NotEmpty String sessionName,
    							   @Idx(index = "2", type = TEXT)  @Pkg(label = "Bucket name" , default_value_type = STRING ) @NotEmpty String bucketName,
    							   @Idx(index = "3", type = TEXT)  @Pkg(label = "Object name" , default_value_type = STRING ) @NotEmpty String objectName
    							   ) throws Exception { 
		

		GoogleStorageBucket storage = (GoogleStorageBucket) this.sessions.get(sessionName);  
		HashMap<String, Value> resultMap = new HashMap<String, Value>();
		
		HashMap<String,Object> details = storage.getObjectDetails(bucketName, objectName);
	    for (Entry<String, Object> entry:details.entrySet() ) {
    		Value value = new StringValue("");;
	    	if  (entry.getValue() != null) {
	    		String className = entry.getValue().getClass().getSimpleName() ;
	    		switch (className) {
	    			case "ZonedDateTime" :  value = new DateTimeValue((ZonedDateTime)entry.getValue());
	    									break;
	    			case "String" : 		value = new StringValue((String)entry.getValue());
											break;
	    			default: 				value = new StringValue(entry.getValue().toString());
											break;					
	    		}
	    	}
	    	resultMap.put(entry.getKey(), value);
	    }
	  
	    DictionaryValue resultDict = new DictionaryValue();
	    resultDict.set(resultMap);
	    
	    return resultDict;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}