/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */


package com.automationanywhere.botcommand.google.storage;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.google.storage.utils.GoogleStorageBucket;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "List Objects", name = "storagelistobjects",
        description = "List Objects",
        node_label = "List Objects {{sessionName}}|", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,
        return_type=DataType.LIST, return_sub_type = DataType.STRING , return_description = "List of objects" , return_label="Objects", return_required=true)

public class ListObjects {
	
	  private static final Logger logger = LogManager.getLogger(ListObjects.class);
	
    @Sessions
    private Map<String, Object> sessions;

	   
	@Execute
    public ListValue<String>  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "StorageSession") @NotEmpty String sessionName,
			   						 @Idx(index = "2", type = TEXT)  @Pkg(label = "Bucket name" , default_value_type = STRING ) @NotEmpty String bucketName,
    								 @Idx(index = "3", type = TEXT)  @Pkg(label = "Folder path" , description = "Optional folder path" ,default_value_type= STRING )  String folderPath
    						 ) throws Exception { 
		
		List<Value> objectsList = new ArrayList<Value>();
		folderPath = (folderPath == null) ? "" : folderPath;
		GoogleStorageBucket storage = (GoogleStorageBucket) this.sessions.get(sessionName);  
		
		List<String> objects = storage.listObjects( bucketName, folderPath); 

		for (Iterator iterator = objects.iterator(); iterator.hasNext();) {
			String object= (String) iterator.next();
			objectsList.add(new StringValue(object));
		}
		
		ListValue<String> resultList = new ListValue<String>();
		resultList.set(objectsList);
		
	    return resultList;

	
	}

	
	public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
   	
	
}